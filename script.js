/**
 * V tomto Javascript souboru nalezneme všechny funkce a funkcionality webové aplikace Knihovna Recenzí knih
 *
 */



/**
 * zobrazení formulářu
 * */
let loginForm = document.querySelector('.login');
let novaRForm = document.querySelector('.pridat');
let zNovaRForm = document.querySelector('#novaR');

document.querySelector('#prihlasB').onclick = () => {
    loginForm.classList.toggle('active');
    novaRForm.classList.remove('active');
}



document.querySelector('#closelogB').onclick = () => {
    loginForm.classList.remove('active');
}



document.querySelector('#novaR').onclick = () => {
    novaRForm.classList.toggle('active');
    loginForm.classList.remove('active');
}

document.querySelector('#closerecB').onclick = () => {
    novaRForm.classList.remove('active');
}


let admin1Jmeno = "admin1";
let admin1Heslo = "cisco";

let admin2Jmeno = "admin2";
let admin2Heslo = "veverka1";





/**
 * Tato funkce kontroluje, jestli byl přihlašovací formulář vyplněn správně. Pokud ano, tak se uživateli
 * objeví tlačítko pro zobrazení formuláře pro přidání nové knižní recenze.
 */
function prihlaseni(){
    const uJmeno = document.getElementById("uName");
    const uHeslo = document.getElementById("uPassw");

    if(uJmeno.value===admin1Jmeno&&uHeslo.value===admin1Heslo || uJmeno.value===admin2Jmeno&&uHeslo.value===admin2Heslo){
        zNovaRForm.classList.toggle('active');
    }

    loginForm.classList.remove('active');
}

let poleRecenzi = [];


/**
 * tato funkce zkontroluje jestli pole recenzí knih je null, pokud ano tak do něj nahraje statické hodnoty
 */
function init(){
    poleRecenzi = JSON.parse(localStorage.getItem("poleRecenzi"));

    if(poleRecenzi===null){
        poleRecenzi=[kniha1,kniha2,kniha3];
        localStorage.setItem("poleRecenzi",JSON.stringify(poleRecenzi));

    }

    nahravani();
}

/**
 * tato funkce nahraje nově přidanou recenzi z lokálního uložiště a zobrazí ji na stránce.
 */
function nahravani(){
    poleRecenzi = JSON.parse(localStorage.getItem("poleRecenzi"));

    poleRecenzi.forEach(ele => {
        zobrazRecenze(ele);
    })
}



let pocetKnih = 3;

const kniha1 = {
    id:1,
    titul:"Krysař",
    autor:"Viktor Dyk",
    recenze:"\"Krysař\" je mistrně napsaná novela, která i přes svou krátkost nabízí hluboký vhled do lidské povahy a společnosti. Viktor Dyk vytvořil nadčasové dílo, které zůstává aktuální i po více než sto letech od svého vydání. Díky své symbolice a poetickému jazyku je \"Krysař\" dílem, které osloví nejen milovníky klasické literatury, ale i ty, kteří hledají příběhy s hlubším poselstvím."
};
const kniha2 = {
    id:2,
    titul:"Pes baskervillský",
    autor:"Arthur Conan Doyle",
    recenze:"\"Pes baskervillský\" je mistrovským dílem detektivního žánru. Arthur Conan Doyle vytvořil příběh, který kombinuje napětí, tajemství a brilantní deduktivní myšlení Sherlocka Holmese. Kniha zůstává fascinující i po více než sto letech od svého vydání, díky své atmosféře a dobře vystavěnému příběhu. \"Pes baskervillský\" je nejen klasika detektivní literatury, ale také skvělý příklad toho, jak lze kombinovat hororové prvky s logickým řešením záhady."
};
const kniha3 = {
    id:3,
    titul:"Společenstvo prstenu",
    autor:"J.R.R. Tolkien",
    recenze:"\"Společenstvo prstenu\" je epické fantasy dílo, které nabízí fascinující příběh, komplexní svět a nezapomenutelné postavy. Tolkienova schopnost vytvořit hluboký a bohatý svět, ve kterém se odehrává boj mezi dobrem a zlem, činí tuto knihu jedním z pilířů moderní fantasy literatury. Příběh o odvaze, přátelství a obětavosti rezonuje s čtenáři všech věkových kategorií a zůstává relevantní i mnoho let po svém vydání."
};


/**
 * Tato funkce vytvoří section, do kterého přidá titul, autora a recenzi knihy, společně s formulářem
 * pro přidání komentářů a hodnocení pod recenzi. Také přidá prázdný seznam, ve kterém se zobrazí
 * přidané komentáře a hodnocení recenze.
 *
 * @param kniha     objekt, ze kterého bere zobrazené informace – titul, autora a recenzi knihy
 */
function zobrazRecenze(kniha){

    const komentare = [];
    const rec = document.querySelector('.rZobrazeni');

    const sec = document.createElement('section');
    rec.append(sec);

    const rTitul = document.createElement('h2');
    rTitul.textContent = "titul: " + kniha.titul;
    sec.append(rTitul);

    const rAutor = document.createElement('h3');
    rAutor.textContent = "autor: " + kniha.autor;
    sec.append(rAutor);

    const rRecenze = document.createElement('p');
    rRecenze.textContent = "recenze: " + kniha.recenze;
    sec.append(rRecenze);




    const kT = document.createElement('input');
    kT.setAttribute('type','text');
    sec.append(kT);

    const kS = document.createElement('input');
    kS.setAttribute('type','number');
    kS.min=1;
    kS.max=5;
    sec.append(kS);

    const kB = document.createElement('button');
    kB.textContent='Přidat komentář';
    sec.append(kB);

    const ul = document.createElement('ul');
    sec.append(ul);
    listKomentaru(komentare);

    kB.onclick = function(){pridejK(komentare,kT,kS,ul);};

}

/**
 * Tato funkce vytvoří nový komentář, jako string, a uloží ho do pole.
 *
 * @param arr       pole, do kterého se komentář uloží
 * @param t         hodnota, převzatá z formuláře pro přidání komentáře
 * @param s         hodnota, převzatá z formuláře pro přidání hodnocení
 * @param ul        seznam, který zobrazí pole komentářů
 */
function pridejK(arr,t,s,ul){
    const novyK = s.value + "/5 hvězd; " + t.value;
    arr.push(novyK);
    pridejKDoListu(novyK,ul);
}

/**
 * Tato funkce vytvoří novou položku v seznamu a přidá do ní komentář.
 *
 * @param k         string, komentář, který přidáváme do seznamu
 * @param ul        seznam, do kterého přidáváme komentář
 */
function pridejKDoListu(k,ul){
    const li = document.createElement('li');
    li.textContent = k;
    ul.append(li);
}

/**
 * Tato funkce předává všechny prvky z pole do seznamu.
 *
 * @param arr       pole, ze kterého seznam převezme hodnoty
 * @param ul        seznam, který převezme hodnoty z pole
 */
function listKomentaru(arr,ul){
    arr.forEach(ele => {
        pridejKDoListu(ele,ul);
    })
}

/**
 * Tato funkce přebírá hodnoty z formuláře pro tvorbu nové recenze, aby mohli být zobrazeny na stránce.
 * Nová recenze je následně uložena do lokálního uložiště.
 */
function novaRecenze(){
    const titulKnihy = document.getElementById("titulT");
    const autorKnihy = document.getElementById("autorT");
    const recenzeKnihy = document.getElementById("recenzeT");

    pocetKnih++;
    const novaKniha = {};
    novaKniha.id = pocetKnih;
    novaKniha.titul = titulKnihy.value;
    novaKniha.autor = autorKnihy.value;
    novaKniha.recenze = recenzeKnihy.value;
    poleRecenzi.push(novaKniha);
    localStorage.setItem("poleRecenzi",JSON.stringify(poleRecenzi));
    zobrazRecenze(novaKniha);
    novaRForm.classList.remove('active');
}


